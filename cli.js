#!/usr/bin/env node

const fs = require('fs');
const excelToJson = require('convert-excel-to-json');
const xlsx = require('node-xlsx');

const [,,...args] = process.argv;

if(args.length === 0 || !args[0].replace('.\\','').includes('.')) {
    console.error('File does not exists');
    process.exit();
}

const fileName = args[0].replace('.\\','').split('.')[0];
const fileExt = args[0].replace('.\\','').split('.')[1];
const file = args[0].replace('.\\','');
const outputFile = fileName + '.csv';

const pattern1 = /\[S\]/g;
const pattern2 = /\[RT\]/g;
const colourPattern = /{(.*)}/;
const measurementPattern = /[m+|M+].*[x|X]+/;

if (fileExt.toUpperCase() === 'XLSX') {
    if(!fs.existsSync(file)) {
        console.error('File does not exists');
        process.exit();
    }

    if (fs.existsSync(outputFile)) {
        fs.unlinkSync(outputFile);
    }

    xlsx.parse(file)[0].data.forEach(lineItems => {
        const originalValue = lineItems[2];

        if (originalValue && originalValue.length > 0 &&
            (originalValue.match(pattern1) || originalValue.match(pattern2))) {

            let tempValue = originalValue.slice(0);

            let type = '';
            tempValue = originalValue.replace('Size=','');

            if(tempValue.includes('[RT]')) {
                tempValue = tempValue.replace('[RT]', '');
                type = 'rectangles';
            } else if(tempValue.includes('[S]')) {
                type = 'dropdown';
                tempValue = tempValue.replace('[S]', '');
            }

            let initialState = {
                "sort_order": 1,
                "variation_info": []
            };

            let variant = {
                    "label": '',
                    "value": '',
                    "type": ''
                };

            // Working on Colour
            if (tempValue.match(colourPattern)) {
                let tempVariant = Object.assign({}, variant);
                tempVariant.label = 'Colour';
                tempVariant.value = tempValue.match(colourPattern)[1];
                tempVariant.type = type;
                initialState.variation_info.push(Object.assign({}, tempVariant));
                tempValue = tempValue.replace(tempValue.match(colourPattern)[0], '');
            }

            let tempArray = tempValue.split(' ');

            // Working on Size
            if (tempArray.length > 0 && tempArray[0] !== '') {
                if (tempArray[0].match(measurementPattern)) {
                    let tempVariant = Object.assign({}, variant);
                    tempVariant.label = 'Size';
                    tempVariant.value = tempArray[0];
                    tempVariant.type = type;
                    initialState.variation_info.push(Object.assign({}, tempVariant));
                    tempArray.shift();
                }
            }

            // Working on Option
            if (tempArray.length > 0 && tempArray[0] !== '') {
                let tempVariant = Object.assign({}, variant);
                tempVariant.label = 'Option';
                tempVariant.value = tempArray[0];
                tempVariant.type = type;
                initialState.variation_info.push(Object.assign({}, tempVariant));
                tempArray.shift();
            }

            // console.log('original: ' + originalValue);
            // console.log(JSON.stringify(initialState));
            lineItems[2] = '"' + JSON.stringify(initialState).replace(/"/g, '""') + '"';
            fs.appendFileSync(outputFile, lineItems.join(',') + '\n');
        } else {
            fs.appendFileSync(outputFile, lineItems.join(',') + '\n');
        }
    });
} else {
    console.error('File must end with XSLX extension')
}

