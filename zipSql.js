#!/usr/bin/env node

const path = require('path');
const fs = require('fs');
const zlib = require('zlib');
const compressFileOlderThanAgeInDays = 2;
const extensions = ['trn', 'bak'];
const [,,...args] = process.argv;

let filesToCompress = [];

if(args.length === 0) {
    console.error('File does not exists');
    process.exit();
}
const directoryPath = path.join(args[0], '\\');
const deleteFlag = args[1];

console.log('Working directory: ' + directoryPath);

function compressFile(filename, callback) {
    const compress = zlib.createGzip();
    const inputFilename = directoryPath + filename;
    const outputFilename = directoryPath + filename + '.gz';

    let input = fs.createReadStream(inputFilename);
    let output = fs.createWriteStream(outputFilename);

    console.log('Creating ' + outputFilename);

    input.pipe(compress).pipe(output);

    if (deleteFlag === '--delete') {
        if (fs.existsSync(outputFilename)) {
            console.log('Deleting ' + filename);
            fs.unlinkSync(inputFilename);
        } else {
            console.log('Delete flag is set but output file is missing.');
        }
    }

    if (callback) {
        output.on('end', callback);
    }
}

fs.readdir(directoryPath, function (err, files) {
    if (err) {
        return console.log('Unable to scan directory: ' + err);
    }

    files.forEach(function (file) {

        const fileArray = file.split('.');
        const fileExt = fileArray.slice(-1)[0];
        const stats = fs.statSync(directoryPath + file);
        const days = (new Date().getTime() - stats.mtime) / 1000 / 86400;
        if (fileExt !== undefined && extensions.indexOf(fileExt.toLowerCase()) > -1 && days > compressFileOlderThanAgeInDays) {
            filesToCompress.push(file);
        }
    });

    console.log('Found files: ' + JSON.stringify(filesToCompress));
    filesToCompress.forEach(file => compressFile(file));
});
